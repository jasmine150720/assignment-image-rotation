#include "rotation.h"
#include <malloc.h>

struct image rotate(struct image const image) {
    struct image rotated = {image.height,
                            image.width,
                            malloc(sizeof(struct pixel) * image.width * image.height)};
    for (size_t y = 0; y < image.height; y++) {
        for (size_t x = 0; x < image.width; x++) {
            *get_pixel(image.height - 1 - y, x, &rotated) = *get_pixel(x, y, &image);
        }
    }
    return rotated;
}
