#include "../include/file.h"
#include "../include/status.h"
#include <errno.h>

enum io_status file_open(FILE** file, const char* name, const char* mode) {
    *file = fopen(file_name, mode);
    if (*file != NULL) return FILE_OPENED_SUCCESSFULLY;
    if (errno == EACCES) return FILE_PERMISSION_DENIED;
    if (errno == ENOENT) return FILE_NOT_EXIST;
    return FILE_OPENED_UNSUCCESSFULLY;
}

enum status file_close(FILE *file) {
    if (fclose(file) == EOF)
        return FILE_CLOSED_UNSUCCESSFULLY;
    return FILE_CLOSED_SUCCESSFULLY;
}

