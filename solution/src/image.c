#include "../include/image.h"
#include <malloc.h>

struct pixel* get_pixel(uint32_t x, uint32_t y, struct image const* const image) {
    return image->data + y*img->width + x;
}

void image_destroy(struct image image) {
    if (image->data != NULL) free(image->data);
}
