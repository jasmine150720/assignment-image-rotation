#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H

#include <bits/types/FILE.h>
#include "status_enum.h"
enum status file_open(const char *file_name, FILE **file, char *mode);

enum status file_close(FILE *file);