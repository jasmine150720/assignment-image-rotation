#ifndef IMAGE_H
#define IMAGE_H

#include <stddef.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel *data;
};

struct image new_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

#endif //IMAGE_H