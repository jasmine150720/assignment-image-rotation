
#ifndef ROTATION_H
#define ROTATION_H

#include <image.h>

struct image *rotation(struct image const *source);

#endif //ROTATION_H