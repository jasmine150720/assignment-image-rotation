#ifndef STATUS_H
#define STATUS_H

enum status {
    SUCCESS = 0,
    FILE_OPEN_ERROR,
    FILE_CLOSE_ERROR,
    READ_ERROR,
    WRITE_ERROR,
};

#endif //STATUS_H